# PufferfishPlayground

Just dabbling with interactive OpenBSD post-install shortcuts. 

Target audience is the occasional OpenBSD user with Linux background (aka "me").

I use OpenBSD only on rather old and possibly ppc32/non-x86 machines so there's a minimalism paradigm at work here.

# Usage

Run as root (if you dare...):

```bash
curl -L "https://codeberg.org/Rene.Schmidt/PufferfishPlayground/raw/branch/master/pufferfish-post-install.ksh" --output - | ksh
```

Then follow the prompts.

## dwm keyboard shortcuts

In this example, key "Mod1" is <kbd>left-alt</kbd>. May be different on your machine.

| Keyboard shortcut | Description
| ---- | -----------
| <kbd>left-ctrl + shift + tab</kbd> | Show dmenu in top bar
| <kbd>left-alt + shift + enter</kbd> | Open terminal
| <kbd>left-alt + p, "st", enter</kbd> | Start "st"
| <kbd>left-alt + m</kbd> | Maximize window
| <kbd>left-alt + space</kbd> | Un-maximize window
| <kbd>left-alt + shift + 2</kbd> | Move window to screen 2
| <kbd>left-alt + 2</kbd> | Move view to screen 2

# VirtualBox specifics 

## Set virtual screen size

Run when the VM is offline in order to set the virtual screen size:

```bash
VBoxManage setextradata NameOfVm CustomVideoMode1 1920x1080x24
```

## Set hardware clock to UTC

Otherwise the date may be wrong after installation.

# Create USB stick for install

1. Download `SHA256SUM` and `installXY.img` from https://cdn.openbsd.org/pub/OpenBSD/X.Y/amd64/ and run:

```bash
sha256sum -c SHA256 # check for "installXY.img: OK"
sudo dd if=installXY.img of=/dev/sdX bs=1M # replace "sdX" with the actual USB device
```

# Install GNOME

Inspired by https://functionallyparanoid.com/2020/10/14/openbsd-laptop/

```bash
rcctl enable apmd
rcctl set apmd flags -A
rcctl start apmd
usermod -G staff <user>
```

Edit ` /etc/login.conf`:

``` 
staff:\
        :coredumpsize=0:\
        :datasize-cur=infinity:\
        :datasize-max=infinity:\
        :datasize=infinity:\
        :openfiles-cur=2048:\
        :stacksize-cur=32M:\
        :maxproc-max=1024:\
        :maxproc-cur=1024:\
        :ignorenologin:\
        :requirehome@:\
        :tc=default:
```

Edit `/etc/sysctl.conf`:

```
kern.maxfiles=102400
kern.shminfo.shmall=536870912 # for 16 gigs RAM
```

```bash
rcctl stop xenodm
pkg_delete dwm dmenu dstat st 
rcctl disable xenodm

pkg_add gnome \
&& pkg_add gnome-tweaks \
&& pkg_add gnome-extras

rcctl enable multicast messagebus avahi_daemon gdm

pkg_add firefox \
&& pkg_add chromium \
&& pkg_add seafile-client \
&& pkg_add keepassxc \
&& pkg_add gimp 
```

Edit `/etc/gdm/locale.conf`:

```
LC_CTYPE="de_DE.UTF-8"
LC_MESSAGES="de_DE.UTF-8"
```
