#!/bin/ksh

bail_on_non_root() {
  CURRENT_USER=$(whoami)
  if [[ "${CURRENT_USER}" != "root" ]]; then
    print "Must be run as root. Run 'su;cp /etc/examples/doas.conf /etc/doas.conf;exit', then try again: 'doas $0'"
    exit 1
  fi
  cp /etc/examples/doas.conf /etc/doas.conf
}

install_dialog() {
  DIALOG_INSTALLED=$(pkg_info dialog >/dev/null)
  if [[ $DIALOG_INSTALLED != 0 ]]; then
    pkg_add dialog
  fi
}

troll() {
  dialog \
    --title "Question" \
    --defaultno \
    --yesno "Pufferfish Post Install assumes that\n\n1. you have enabled network and SSH access\n2. created a user account\n\nDid you do that?" \
    10 \
    50
  RET=$?
  clear

  if [[ $RET != 0 ]]; then
    print "OK please reinstall and then try again."
    exit 1
  fi
}

install_packages() {
  pkg_add -Iv dwm dmenu mc st htop git nano micro wget xbindkeys dstat neofetch
}

install_polyglot() {
  if [[ -d /usr/local/share/polyglot ]]; then
    cd /usr/local/share/polyglot || exit
    git config pull.rebase false
    git fetch
    git pull
  else
    cd /usr/local/share/ || exit
    git clone https://github.com/agkozak/polyglot
  fi
}

# Appends a line to a file if it does not contain the line yet.
append_line_to_file_once() {
  line="$1"
  file="$2"
  grep -qxF "${line}" "${file}" || echo "${line}" >>"${file}"
}

configure_user_shell() {
  dialog \
    --title "Question" \
    --defaultno \
    --yesno "Configure kshrc for users?" \
    10 \
    50
  response=$?
  clear

  if [[ $response == 0 ]]; then
    while true; do
      local_user=$(dialog --inputbox "Local user name? (enter nothing to skip) " 0 0 "$(whoami)" 3>&1 1>&2 2>&3)
      [[ "${local_user}" == "" ]] && break
      if [[ "${local_user}" == "root" ]]; then
        kshrc="/root/.kshrc"
        profile="/root/.profile"
        touch "${kshrc}"
      else
        kshrc="/home/${local_user}/.kshrc"
        profile="/home/${local_user}/.profile"
        touch "${kshrc}"
        chown "${local_user}:${local_user}" "${kshrc}"
      fi

      append_line_to_file_once "ENV=.kshrc" "${profile}"
      append_line_to_file_once "export ENV" "${profile}"
      append_line_to_file_once "POLYGLOT_SH=/usr/local/share/polyglot/polyglot.sh" "${kshrc}"
      append_line_to_file_once "[[ -f \"\${POLYGLOT_SH}\" ]] && . \"\${POLYGLOT_SH}\" || print \"\${POLYGLOT_SH} not found\"" "${kshrc}"
      append_line_to_file_once "alias micro='TERM=xsterm-256color micro'" "${kshrc}"
      dialog --msgbox "Shell set up for user '${local_user}'" 5 30
    done
  fi
}

# note: not idempotent
configure_ssh_pubkeys() {
  dialog \
    --title "Question" \
    --defaultno \
    --yesno "Import SSH pubkeys from GitHub?" \
    10 \
    50
  response=$?
  clear

  if [[ $response == 0 ]]; then
    while true; do
      local_user=$(dialog --inputbox "Local user name? (enter nothing to skip) " 0 0 "$(whoami)" 3>&1 1>&2 2>&3)
      [[ "${local_user}" == "" ]] && break
      if [[ "${local_user}" == "root" ]]; then
        authorized_keys_file="/root/.ssh/authorized_keys"
      else
        authorized_keys_file="/home/${local_user}/.ssh/authorized_keys"
      fi
      [[ ! -f "${authorized_keys_file}" ]] && print "${authorized_keys_file} does not exist" && break
      github_user=$(dialog --inputbox "GitHub user name?" 0 0 "$(whoami)" 3>&1 1>&2 2>&3)
      wget "https://github.com/${github_user}.keys" -O - >>"${authorized_keys_file}"
    done
  fi
}

configure_hyperthreading() {
  dialog \
    --title "Question" \
    --defaultno \
    --yesno "Enable hyperthreading?" \
    10 \
    50
  response=$?
  clear

  if [[ $response == 0 ]]; then
    if [[ ! -f /etc/sysctl.conf ]]; then
      cp /etc/examples/sysctl.conf /etc/sysctl.conf
    fi
    append_line_to_file_once "hw.smt=1" /etc/sysctl.conf
    touch /etc/boot.conf
    append_line_to_file_once "boot bsd.mp" /etc/boot.conf
  fi
}

configure_xorg() {
  dialog \
    --title "Question" \
    --defaultno \
    --yesno "Configure x.org?" \
    10 \
    50
  response=$?
  clear

  if [[ $response == 0 ]]; then
    if [[ ! -f /etc/sysctl.conf ]]; then
      cp /etc/examples/sysctl.conf /etc/sysctl.conf
    fi
    append_line_to_file_once "machdep.allowaperture=2" /etc/sysctl.conf
    if [[ -d /etc/X11/xenodm ]]; then
      touch /etc/X11/xenodm/Xresources
      append_line_to_file_once "xlogin*borderWidth: 0" /etc/X11/xenodm/Xresources
      append_line_to_file_once "xlogin*frameWidth: 0" /etc/X11/xenodm/Xresources

      cat <<EOF >/etc/X11/xenodm/Xsetup_0
#!/bin/sh

BG_COLOR=\$(xrdb -query | awk '/xlogin.background/ { print \$2 }')
xsetroot -solid \$BG_COLOR
EOF

      if [[ "$(sysctl -n hw.product)" == "VirtualBox" ]]; then
        cat <<EOF >/usr/X11R6/share/X11/xorg.conf.d/00-virtualbox-monitor.conf
Section "Device"
  Identifier   "VirtualBox-Card"
  Driver       "vmware"
  VendorName   "InnoTek"
  BoardName    "VirtualBox Graphics Adapter"
EndSection
Section "Monitor"
  Identifier   "VirtualBox-Monitor"
  VendorName   "InnoTek"
  ModelName    "VirtualBox Screen"
  HorizSync    1.0 - 1000.0
  VertRefresh  1.0 - 1000.0
EndSection
Section "Screen"
  Identifier   "VirtualBox-Screen"
  Device       "VirtualBox-Card"
  Monitor      "VirtualBox-Monitor"
  DefaultDepth 24
  SubSection "Display"
    Viewport   0 0
    Depth      24
    Modes  "1920x1080"
  EndSubSection
EndSection
EOF
    fi
  fi
  while true; do
    local_user=$(dialog --inputbox "Enter local user name to set up X.org for (enter nothing to skip) " 0 0 "" 3>&1 1>&2 2>&3)
    if [[ "${local_user}" == "" ]]; then
      break
    elif [[ "${local_user}" == "root" ]] || [[ ! -d "/home/${local_user}/" ]]; then
      dialog --msgbox "Illegal user" 5 30
    else
      xbindkeys --defaults >"/home/${local_user}/.xbindkeysrc"

      cat <<EOF >>"/home/${local_user}/.xbindkeysrc"
"dmenu_run"
   m:0x5 + c:23
   Control+Shift + Tab
EOF

        chown "${local_user}:${local_user}" "/home/${local_user}/.xbindkeysrc"
        cat <<EOF >"/home/${local_user}/.xsession"
#!/bin/sh
xsetroot -solid "#555"
xset -b
[[ -f "/home/${local_user}/.Xresources" ]] && xrdb -merge /home/${local_user}/.Xresources
[[ -f "/home/${local_user}/.Xmodmap" ]] && xmodmap /home/${local_user}/.Xmodmap
setxkbmap -layout de -option ctrl:nocaps
export ENV=/home/${local_user}/.kshrc
#dstat <if> &
xbindkeys --poll-rc
. /home/${local_user}/.profile
exec dwm
EOF
        chown "${local_user}:${local_user}" "/home/${local_user}/.xsession"
        dialog --msgbox "X.org set up for user '${local_user}'" 5 30
      fi
    done

    rcctl enable xenodm
    rcctl restart xenodm
  fi
}

bail_on_non_root
install_dialog
troll
install_packages
install_polyglot
configure_user_shell
configure_ssh_pubkeys
configure_xorg
configure_hyperthreading

# There is more to come.

# read response
# if [[ -z $response ]] || [[ "${response}" != "y" ]] ; then
#        print "msg"
#        exit 1
# fi
